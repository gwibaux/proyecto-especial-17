<article class="container" >
    <section class="row container">
        {foreach from=$categorias item=categoria}
        <div class="col-lg-8 card">
            <div class="card-block">
              <h4 class="card-title">{$categoria['name']}</h4>
              <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
              <a href="#" class="card-link">Card link</a>
              <a href="#" class="card-link">Another link</a>
            </div>
          </div>
      {/foreach}
    </section>
  </article>
