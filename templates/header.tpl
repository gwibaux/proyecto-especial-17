<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>{{$title}}</title>

	<!-- Bootstrap CSS -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="js/navLogic.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"
	 crossorigin="anonymous">
	 
	<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" href="css/style.css" type="text/css">

</head>

<body>

	<header>

		<!--  Carousel Header  -->
		<div id="carousel-header" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carousel-header" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-header" data-slide-to="1"></li>
				<li data-target="#carousel-header" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img class="d-block w-100" src="./images/header/header-image-2.jpg" alt="First slide">
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="./images/header/header-image-6.jpg" alt="Second slide">
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="./images/header/header-image-4.jpg" alt="Third slide">
				</div>
			</div>
			<a class="carousel-control-prev" href="#carousel-header" role="button" data-slide="prev">
				  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				  <span class="sr-only">Previous</span>
				</a>
			<a class="carousel-control-next" href="#carousel-header" role="button" data-slide="next">
				  <span class="carousel-control-next-icon" aria-hidden="true"></span>
				  <span class="sr-only">Next</span>
				</a>
		</div>

	</header>


	<!--  Navbar  -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">Sonic-Commerce</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
		 aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"></span>
			</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link navegacion" id="home" href="#">Home <span class="sr-only">(current)</span></a>
				</li>				
				<li class="nav-item">
					<a class="nav-link navegacion" id="categorias" href="#">Categories</a>
				</li>
				<li class="nav-item">
					<a class="nav-link navegacion" id="products" href="#">Products</a>
				</li>
				<li class="nav-item">
					<a class="nav-link navegacion" id="contact" href="#">Contact</a>
				</li>
				
				{if $session}
					<li class="nav-item">
						<a class="nav-link navegacion" id="admin" href="#">Admin</a>
					</li>  	
				{else}
					<li class="nav-item">
						<a class="nav-link navegacion" id="login" href="#">Login</a>
					</li>
			  	{/if}
			</ul>
		</div>



		<div class="col user">
			<i class="fa fa-cog" aria-hidden="true"></i>
			<i class="fa fa-bell" aria-hidden="true"></i>
			<img id="showUser" src="./images/avatar-user.png" class="rounded-circle img-header">
			<div id="userDropdown" class="dropdown">

				<div class="profile">
					<div class="img-dropdown-container">
						<img src="./images/avatar-user.png" class="rounded-circle img-dropdown">
					</div>
					<div class="text-container">
						<p class="text">Nombre Apellido</p>
						<p class="text">user@domain.com</p>
					</div>
				</div>

				<div class="user-footer">
					<button class="btn btn-secondary btn-sm" type="button">Another action</button>
					<button id="log-out" class="btn btn-secondary btn-sm">Logout</button>
				</div>

			</div>

		</div>
	</nav>