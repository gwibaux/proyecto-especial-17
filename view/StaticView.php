<?php
class StaticView extends View
{
  function showIndex($status){
    $this->smarty->assign('session',$status);
     $this->smarty->display('templates/index.tpl');
     }

    function mostrarHome(){
      $this->smarty->display('templates/home.tpl');
    }

    function showContact(){
      $this->smarty->display('templates/contact.tpl');
    }

    function mostrarProductos(){
      $this->smarty->display('templates/productos.tpl');
    }

    function mostrarPanel(){
      $this->smarty->display('templates/panelAdmin.tpl');
    }

  }

 ?>
