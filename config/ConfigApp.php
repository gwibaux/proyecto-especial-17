<?php

class ConfigApp
{
    public static $ACTION = 'action';
    public static $PARAMS = 'params';
    public static $ACTIONS = [
      ''=> 'StaticController#index',
      'home'=> 'StaticController#home',
      'login' => 'LoginController#login',
      'logout' => 'LoginController#cerrarSesion',
      'admin' => 'AdminController#index',
      'contact' => 'StaticController#contact',   
      'products' => 'ProductController#products',
      'producto' => 'Producto#detalle',
      'categorias' => 'CategoriaController#categoria',
      
      'admin' => 'AdminController#admin',
      
      
      // 'addCategory'=> 'CategoriaController#addCategory',
      // 'borrarCategoria'=> 'CategoriaController#deleteCat',
      // 'editarCategoria' => 'CategoriaController#editCat',
      // 'listaDelantales' => 'ProductoController#mostarListaDel',
      // 'agregarDelantal'=> 'ProductoController#createDel',
      // 'borrarDelantal'=> 'ProductoController#deleteDel',
      // 'editarDelantal' => 'ProductoController#editDel',

      'mostrarEditarCategoria' => 'AdminCategoriaController#showEditCat',
      'listarCategoria' => 'AdminCategoriaController#mostrarCategoria',
      'agregarCategoria'=> 'AdminCategoriaController#createCat',
      'borrarCategoria'=> 'AdminCategoriaController#deleteCat',
      'editarCategoria' => 'AdminCategoriaController#editCat',
      
      'autenticacion' => 'LoginController#autenticar',
      
      // 'adminCategoria'=>'AdminController#categoria',

    ];

}

 ?>
