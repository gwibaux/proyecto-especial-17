<?php
  include_once('view/StaticView.php');

  class StaticController extends Controller
  {
  function __construct(){
      $this->view = new StaticView();
    }

    public function index(){
      session_start();
      $status = FALSE;
      if(isset($_SESSION['LOGGED']) && $_SESSION['LOGGED']){
        $status = $_SESSION['LOGGED'];
        
      }
      $this->view->showIndex($status);
    }

    // public function index(){
    //   $this->view->mostrarIndex();
    // }
    public function home(){
      $this->view->mostrarHome();
    }
    public function contact(){
      $this->view->showContact();
    }
    public function panel(){
      $this->view->mostrarPanel();
    }
}
 ?>
