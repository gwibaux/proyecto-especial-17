<?php
include_once('model/LoginModel.php');
include_once('view/LoginView.php');

class LoginController extends Controller
{

  function __construct()
  {
    $this->view = new LoginView();
    $this->model = new LoginModel();
  }

  public function login()
  {
    
    $this->view->mostrarLogin();
  }

  public function autenticar()
  {
      $usuario = $_POST['usuario'];     
      $mail = $_POST['mail'];
      
      

      if((!empty($usuario)) && (!empty($mail))){
      

         $data = $this->model->getUser($usuario);        
         $hash = password_hash($_POST['password'], PASSWORD_DEFAULT);

        if((!empty($data)) && (password_verify($data[0]['password'], $hash))){
            session_start();
            $_SESSION['USER'] = $data;
            $_SESSION['LAST_ACTIVITY'] = time();
            $_SESSION['LOGGED'] = true;
            header('Location: '.HOME);
        }
        else{
            $this->view->mostrarLogin('Los datos ingresados son incorrectos');
        }
      }
  }

  public function cerrarSesion()
  {
    session_start();
    session_destroy();
    header('Location: '.LOGIN);
  }
}

 ?>
